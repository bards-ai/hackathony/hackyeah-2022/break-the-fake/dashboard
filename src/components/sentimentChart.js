import React from "react";
import {Chart} from "react-charts";

export default function Bar({dataList}) {
    const list = Object.entries(dataList.filter(item => {
        try {
            new Date(item.date).toISOString();
            return true;
        } catch (e) {
            return false;
        }
    }).sort(function (a, b) {
        return new Date(b.date) - new Date(a.date);
    }).map(item => new Date(item.date).toDateString())
        .reduce((a, e) => { a[e] = a[e] ? a[e] + 1 : 1; return a }, {}))
        .map(([k,v]) => ({
            primary: new Date(k),
            secondary: Number(v),
        }))

    const data = [{
        label: 'counter',
        data: list
    }]
    // const data1p = {
    //     label: 'bankier',
    //     data: dataList.filter(item => item.source === "bankier").map(item => ({
    //         primary: new Date(item.date),
    //         secondary: item.sentiment * item.sentiment_label === "NEGATIVE" ? -1 : 1
    //     }))
    //         .filter(item => item.primary - Date.parse("2022-11-09") > 0)
    //         .sort(function (a, b) {
    //             // Turn your strings into dates, and then subtract them
    //             // to get a value that is either negative, positive, or zero.
    //             return new Date(b.primary) - new Date(a.primary);
    //         }),
    // }
    //
    // // const data2p = {
    // //     label: 'money',
    // //     data: data2.map(item => ({
    // //         primary: new Date(item.date),
    // //         secondary: item.is_fake
    // //     })).filter(item => item.primary - Date.parse("2022-11-10") > 0)
    // //         .sort(function (a, b) {
    // //             return new Date(b.primary) - new Date(a.primary);
    // //         }),
    // // }
    //
    // const data3p = {
    //     label: 'businessInsider',
    //     data: dataList.filter(item => item.source === "businessinsider").map(item => ({
    //         primary: new Date(item.date),
    //         secondary: item.sentiment * item.sentiment_label === "NEGATIVE" ? -1 : 1
    //     })).filter(item => item.primary - Date.parse("2022-11-09") > 0)
    //         .sort(function (a, b) {
    //             return new Date(b.primary) - new Date(a.primary);
    //         }),
    // }
    //
    // const data = [data3p, data1p];

    const primaryAxis = React.useMemo(
        () => ({
            getValue: (datum) => datum.primary,
        }),
        []
    );

    const secondaryAxes = React.useMemo
    (
        () => [
            {
                getValue: (datum) => datum.secondary,
            },
        ],
        []
    );
    return (
        <div style={{height: '90%'}}>
            <Chart
                options={{
                    data,
                    primaryAxis,
                    secondaryAxes,
                }}
            />
        </div>
    );
}
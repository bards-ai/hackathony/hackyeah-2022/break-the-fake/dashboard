import * as React from 'react';
import Box from '@mui/material/Box';
import {DataGrid} from '@mui/x-data-grid';

const columns = [
    { field: 'id', headerName: 'ID', width: 90 },
    {
        field: 'title',
        headerName: 'Title',
        width: 150,
        editable: true,
    },
    {
        field: 'summary',
        headerName: 'Abstract',
        width: 150,
        editable: true,
    },
    {
        field: 'link',
        headerName: 'Link',
        width: 150,
        editable: true,
    },
    {
        field: 'source',
        headerName: 'Source',
        width: 150,
        editable: true,
    },
    {
        field: 'date',
        headerName: 'Date',
        width: 150,
        editable: true,
    },
    {
        field: 'author',
        headerName: 'Author',
        width: 150,
        editable: true,
    },
    {
        field: 'category',
        headerName: 'Category',
        width: 150,
        editable: true,
    },
    {
        field: 'tags',
        headerName: 'Tags',
        width: 150,
        editable: true,
    },
    {
        field: 'is_fake',
        headerName: 'Is Fake',
        width: 150,
        editable: true,
        valueGetter: (params) => `${Number(params.row.is_fake).toPrecision(3)}`
    },
    {
        field: 'sentiment_label',
        headerName: 'Sentiment label',
        width: 150,
        editable: true,
    },
    // {
    //     field: 'fullName',
    //     headerName: 'Full name',
    //     description: 'This column has a value getter and is not sortable.',
    //     sortable: false,
    //     width: 160,
    //     valueGetter: (params) =>
    //         `${params.row.firstName || ''} ${params.row.lastName || ''}`,
    // },
];

export default function DataGridDemo({data}) {
    return (
        <Box sx={{ height: 400, width: '100%' }}>
            <DataGrid
                rows={data.map((item, i) => ({...item, id: i}))}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
                disableSelectionOnClick
                experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
    );
}
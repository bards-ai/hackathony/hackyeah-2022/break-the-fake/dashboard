import React from "react";
import {Chart} from "react-charts";

export default function PopularTagChart({dataList}) {
    const list = Object.entries(dataList.flatMap(item => item.tags)
        .reduce((a, e) => { a[e] = a[e] ? a[e] + 1 : 1; return a }, {}))
        .sort(([k1,v1], [k2, v2]) => v2 - v1)
        .slice(0, 10)
        .map(([k,v]) => (    {
            primary: k,
            secondary: v,
        }))
    const data = [{
        label: 'tags',
        data: list
    }]
    const primaryAxis = React.useMemo(
        () => ({
            getValue: (datum) => datum.primary,
        }),
        []
    );

    const secondaryAxes = React.useMemo
    (
        () => [
            {
                getValue: (datum) => datum.secondary,
            },
        ],
        []
    );
    return (
        <div style={{height: '90%'}}>
            <Chart
                options={{
                    defaultColors: [ '#dcc41f', '#ddaacc' ],
                    data,
                    primaryAxis,
                    secondaryAxes,
                }}
            />
        </div>
    );
}
import * as React from 'react';
import Typography from '@mui/material/Typography';

export default function Deposits({dataConcat}) {
    const isFakeAvg = dataConcat
        .map(item => item.is_fake)
        .reduce( ( p, c ) => p + c, 0 ) / dataConcat.length

    const sentimentAvg = dataConcat
        .map(item => item.sentiment)
        .reduce( ( p, c ) => p + c, 0 ) / dataConcat.length

    return (
        <React.Fragment>
            <Typography component="h2" variant="h6" color="primary" gutterBottom>
               Fake possibility average
            </Typography>
            <Typography component="p" variant="h4">
                {isFakeAvg.toPrecision(3)}
            </Typography>
            <Typography color="text.secondary" sx={{ flex: 1 }}>
                on 2022
            </Typography>

            <Typography component="h2" variant="h6" color="primary" gutterBottom>
                Sentiment confidence
            </Typography>
            <Typography component="p" variant="h4">
                {sentimentAvg.toPrecision(3)}
            </Typography>
            <Typography color="text.secondary" sx={{ flex: 1 }}>
                on 2022
            </Typography>
        </React.Fragment>
    );
}
import React from 'react';
import ReactWordcloud from 'react-wordcloud';

export default function SimpleWordcloud({data}) {
    const texts = data.map(item => item.translated_summary).join();
    const list = Object.entries(texts.split(" ")
        .filter(word => word.length > 4)
        .reduce((a, e) => { a[e] = a[e] ? a[e] + 1 : 1; return a }, {}))
        .map(([k,v]) => (    {
            text: k,
            value: v,
        }))

    return <ReactWordcloud words={list}/>
}
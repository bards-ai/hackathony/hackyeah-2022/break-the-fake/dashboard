import React from "react";
import {Chart} from "react-charts";

export default function BarHorizontalChart({dataList}) {
    const list = Object.entries(dataList.map(item => item.hatespeech_label)
        .reduce((a, e) => { a[e] = a[e] ? a[e] + 1 : 1; return a }, {}))
        .map(([k,v]) => (    {
            primary: k,
            secondary: v,
        }))
    const data = [{
        label: 'hatespeech',
        data: list
    }]

    const primaryAxis = React.useMemo(
        () => ({
            position: "left",
            getValue: (datum) => datum.primary,
        }),
        []
    );

    const secondaryAxes = React.useMemo
    (
        () => [
            {
                position: "bottom",
                getValue: (datum) => datum.secondary,
            },
        ],
        []
    );
    return (
        <div style={{height: '90%'}}>
            <Chart
                options={{
                    data,
                    primaryAxis,
                    secondaryAxes,
                }}
            />
        </div>
    );
}
import React from "react";
import ReactSpeedometer from "react-d3-speedometer";

export default function SentimentStatus({data}) {
    const sentiMap = Object.entries(data.map(item => item.sentiment_label)
        .reduce((a, e) => { a[e] = a[e] ? a[e] + 1 : 1; return a }, {}))

    const neg = sentiMap.filter(([k,v]) => k === 'NEGATIVE').map(([k,v]) => v).pop()
    const pos = sentiMap.filter(([k,v]) => k === 'POSITIVE').map(([k,v]) => v).pop()

    return (
        <ReactSpeedometer startColor="#33CC33" endColor="#ff0000" maxValue={100} value={Number((neg / (neg + pos)* 100).toPrecision(3))}/>
    );
}
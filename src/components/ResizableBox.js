import React from "react";
import {ResizableBox as ReactResizableBox} from "react-resizable";


export default function ResizableBox({
                                         children,
                                         width ,
                                         height,
                                         resizable = true,
                                         style = {},
                                         className = "",
                                     }) {
    return (
        <div style={{ marginLeft: 20 }}>
            <div
                style={{
                    width: "auto",
                    background: "white",
                    borderRadius: "0.5rem",
                    boxShadow: "0 30px 40px rgba(0,0,0,.1)",
                    ...style,
                }}
            >
                {resizable ? (
                    <ReactResizableBox width='100%' height='100%'>
                        <div
                            style={{
                                width: "100%",
                                height: "100%",
                            }}
                            className={className}
                        >
                            {children}
                        </div>
                    </ReactResizableBox>
                ) : (
                    <div
                        style={{
                            width: `${width}px`,
                            height: `${height}px`,
                        }}
                        className={className}
                    >
                        {children}
                    </div>
                )}
            </div>
        </div>
    );
}
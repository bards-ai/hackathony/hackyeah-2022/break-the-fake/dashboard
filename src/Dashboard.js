import * as React from 'react';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import DataGridDemo from "./components/table";
import SimpleWordcloud from "./components/wordcloud";
import Bar from "./components/sentimentChart";
import BarHorizontalChart from "./components/hatespeechHorizontalBar";
import Deposits from "./components/avg";
import PopularTagChart from "./components/popularTag";
import SentimentStatus from "./components/sentimentStatus";
import data1 from './data/bankier_articles-30-fake_news.json';
import data2 from './data/money-articles-30-fake_news.json';
import data3 from './data/businessinsider_articles-30-fake_news.json';
import {Input} from "@mui/material";


function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://bards.ai/">
                bards.ai
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const mdTheme = createTheme();

function DashboardContent() {
    const [data, setData] = React.useState([...data1, ...data2,  ...data3]);

    return (
        <ThemeProvider theme={mdTheme}>
            <Box sx={{display: 'flex', width: '100%'}}>
                <CssBaseline/>
                <Box
                    component="main"
                    sx={{
                        backgroundColor: (theme) =>
                            theme.palette.mode === 'light'
                                ? theme.palette.grey[100]
                                : theme.palette.grey[900],
                        flexGrow: 1,
                        height: '100vh',
                        overflow: 'auto',
                    }}
                >
                    <Container maxWidth={false} sx={{mt: 4, mb: 4}}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={12} lg={12}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 50,
                                    }}
                                >
                                    <Input onChange={(event) => {
                                        const freshData = [...data1, ...data2,  ...data3]
                                        const filtered = freshData.filter(item => item.text.toLowerCase().includes(event.target.value.toLowerCase()))
                                        setData(filtered.length === 0 ? freshData : filtered)
                                    }}></Input>
                                </Paper>
                            </Grid>
                            <Grid item xs={12} md={3} lg={3}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 350,
                                    }}
                                >
                                    <SimpleWordcloud data={data}/>
                                </Paper>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 350,
                                    }}
                                >
                                    <div style={{fontSize: '15px'}}>Articles count in time</div>
                                    <Bar dataList={data}/>
                                </Paper>
                            </Grid>
                            <Grid item xs={12} md={3} lg={3}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 350,
                                    }}
                                >
                                    <div style={{fontSize: '15px'}}>Negative sentiment ratio</div>
                                   <SentimentStatus data={data}></SentimentStatus>
                                </Paper>
                            </Grid>
                            <Grid item xs={12} md={3} lg={3}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 350,
                                    }}
                                >
                                    <div style={{fontSize: '15px'}}>Hatespeech label</div>
                                    <BarHorizontalChart dataList={data}></BarHorizontalChart>
                                </Paper>
                            </Grid>
                            <Grid item xs={12} md={3} lg={3}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 350,
                                    }}
                                >
                                    <Deposits dataConcat={data}/>
                                </Paper>
                            </Grid>

                            <Grid item xs={12} md={6} lg={6}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 350,
                                    }}
                                >
                                    <div style={{fontSize: '15px'}}>Top 10 tags</div>
                                    <PopularTagChart dataList={data}></PopularTagChart>
                                </Paper>
                            </Grid>
                            {/* Recent Orders */}
                            <Grid item xs={12}>
                                <Paper sx={{p: 2, display: 'flex', flexDirection: 'column'}}>
                                    <DataGridDemo data={data}/>
                                </Paper>
                            </Grid>
                        </Grid>
                        <Copyright sx={{pt: 4}}/>
                    </Container>
                </Box>
            </Box>
        </ThemeProvider>
    );
}

export default function Dashboard() {
    return <DashboardContent/>;
}